#ifndef ARTISTTABLE_H
#define ARTISTTABLE_H

#include <QtGui>
#include <QTableWidget>
#include <sstream>
#include "util.h"
#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_print.hpp"
#include <fstream>

#define ALL 0
#define ARTIST 1
#define PLAYLIST 2

using namespace std;

class ArtistTable : public QTableWidget
{
    Q_OBJECT
public:
    explicit ArtistTable(QWidget *parent = 0);
    void populate();
    void setNowPlayingIcon(int,string);
    void clearNowPlayingIcon();
};

#endif // ARTISTTABLE_H
