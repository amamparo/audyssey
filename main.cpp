#include <QtGui>

#include "mainwindow.h"


int main(int argv, char **args){
    QApplication app(argv, args);
    app.setApplicationName("Audyssey");
    app.setQuitOnLastWindowClosed(true);
    MainWindow window;
    int x, y;
    QSize windowSize;
    int screenWidth,screenHeight,width,height;
    QDesktopWidget *desktop = new QDesktopWidget;
    screenWidth = desktop->width();
    screenHeight = desktop->height();
    windowSize = window.size();
    width = windowSize.width();
    height = windowSize.height();
    x = (screenWidth-width) / 2;
    y = (screenHeight-height) / 2;
    y -= 50;
    window.move( x, y );
    window.show();
    return app.exec();
}
