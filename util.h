#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <sys/stat.h>
#include <pwd.h>
#include <unistd.h>
#include <stdlib.h>
#include <sstream>
#include <fstream>
#include <stdio.h>

using namespace std;

inline static bool fileExists(const std::string& filename){
    struct stat buf;
    if (stat(filename.c_str(), &buf) != -1){
        return true;
    }
    return false;
}

/***************************************
 * get path to user's library xml file *
 ***************************************/
inline static string getXmlPath(){
    register struct passwd *pw;
    register uid_t uid;
    uid = geteuid ();
    pw = getpwuid (uid);
    stringstream ss;
    ss << "/Users/" << pw->pw_name << "/Music/Audyssey/";
    mkdir(ss.str().c_str(), S_IRWXG | S_IRWXO | S_IRWXU);
    ss << "Audyssey_library.xml";
    return ss.str();
}

//return true if string b exists in string a
inline static bool stringContains(string a, string b){
    if(strlen(a.c_str()) < strlen(b.c_str())){
        return false;
    } else if(strlen(a.c_str()) == strlen(b.c_str())){
        if(strcasecmp(a.c_str(),b.c_str())){
            return false;
        } else return true;
    } else {
        int len_diff = strlen(a.c_str()) - strlen(b.c_str());
        int b_len = strlen(b.c_str());
        stringstream ss;
        string substr;
        for(int i=0; i<=len_diff; i++){
            ss.str("");
            for(int j=i; j<i+b_len; j++){
                ss << a.at(j);
            }
            substr = ss.str();
            if(strcasecmp(b.c_str(),substr.c_str())==0){
                return true;
            }
        }
        return false;
    }
}

#endif // UTIL_H
