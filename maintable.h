#ifndef MAINTABLE_H
#define MAINTABLE_H

#include <QtGui>
#include <QTableWidget>
#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_print.hpp"
#include "taglib/fileref.h"
#include "taglib/tag.h"
#include "util.h"

#define ALL 0
#define ARTIST 1
#define PLAYLIST 2

class MainTable : public QTableWidget
{
    Q_OBJECT
public:
    explicit MainTable(QWidget *parent = 0);
    void populate();
    void filter(QString);
    void filterByArtist(QString);
    QString nowPlayingSource;
    int currentContext;
    int nowPlayingContext;
    void setNowPlayingIcon(string);
    void clearNowPlayingIcon();
};

#endif // MAINTABLE_H
