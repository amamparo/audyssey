#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QtCore/QCoreApplication>
#include <phonon/audiooutput.h>
#include <phonon/seekslider.h>
#include <phonon/mediaobject.h>
#include <phonon/backendcapabilities.h>
#include <QLabel>
#include <QPushButton>
#include <QRect>
#include "maintable.h"
#include "browser.h"
#include "titlebar.h"

class QAction;
class QTableWidget;

class MainWindow : public QFrame{
    Q_OBJECT
public:
    MainWindow();
    QSize sizeHint() const {
        return QSize(500, 300);
    }
    void mousePressEvent(QMouseEvent *e){
        m_old_pos = e->pos();
        m_mouse_down = e->button() == Qt::LeftButton;
    }
    void mouseMoveEvent(QMouseEvent *e){
        int x = e->x();
        int y = e->y();
        if (m_mouse_down) {
            int dx = x - m_old_pos.x();
            int dy = y - m_old_pos.y();
            QRect g = geometry();
            if(left){
                g.setLeft(g.left() + dx);
            }
            if(right){
                g.setRight(g.right() + dx);
            }
            if(bottom){
                g.setBottom(g.bottom() + dy);
            }
            setGeometry(g);
            m_old_pos = QPoint(!left ? e->x() : m_old_pos.x(), e->y());
        } else {
            QRect r = rect();
            left = qAbs(x - r.left()) <= 5;
            right = qAbs(x - r.right()) <= 5;
            bottom = qAbs(y - r.bottom()) <= 5;
            bool hor = left | right;
            if(hor && bottom){
                if(left){
                    setCursor(Qt::SizeBDiagCursor);
                }else{
                    setCursor(Qt::SizeFDiagCursor);
                }
            }else if(hor){
                    setCursor(Qt::SizeHorCursor);
            }else if(bottom){
                    setCursor(Qt::SizeVerCursor);
            }else{
                    setCursor(Qt::ArrowCursor);
            }
        }
    }
    void mouseReleaseEvent(QMouseEvent*){
        m_mouse_down = false;
    }
private:
    void setupUi();
    void setupActions();
    void setupMenus();
    void importSong(string);
    void changeNowPlayingLabel();
    QPushButton *showBrowserButton;
    //QLineEdit *searchBar;
    MainTable *mainTable;
    QLabel *contextLabel;
    Phonon::MediaObject *mediaObject;
    Phonon::AudioOutput *audioOutput;
    Phonon::SeekSlider *seekSlider;
    ArtistTable *artistTable;
    QLineEdit *searchBar;
    QAction *addFolderAction;
    QAction *exitAction;
    QAction *aboutAction;
    QAction *playAction;
    QAction *prevAction;
    QAction *nextAction;

    TitleBar *titleBar;
    QPoint m_old_pos;
    bool m_mouse_down;
    bool left, right, bottom;
    QScrollArea *mainScrollBar;
    QScrollArea *artistScrollBar;

private slots:
    void play_pause();
    void addFolder();
    void about();
    void mainTableClicked(int row, int col);
    void mainTableDoubleClicked(int row, int column);
    void stateChanged(Phonon::State,Phonon::State);
    //void search();
    //void searchReturnPressed();
    void artistTableClicked(int row, int column);
    //void showBrowser();
    void mediaObjectFinished();
    void updateMainScrollBarRange(int min, int max){
        mainScrollBar->verticalScrollBar()->setRange(min,max);
    }
    void updateArtistScrollBarRange(int min, int max){
        artistScrollBar->verticalScrollBar()->setRange(min,max);
    }
    void windowMaximized(){
        if (titleBar->maxNormal) {
            //move scrollbars outward
            int newPos = QApplication::desktop()->width();
            mainScrollBar->move(newPos-232,0);
            if(mainTable->currentContext==ALL){
                mainScrollBar->setFixedHeight(QApplication::desktop()->height() - 89);
            } else {
                mainScrollBar->setFixedHeight(QApplication::desktop()->height() - 106);
            }
            artistScrollBar->setFixedHeight(QApplication::desktop()->height() - 89);
        } else {
            //move scrollbars back
            mainScrollBar->move(792,0);
            if(mainTable->currentContext==ALL){
                mainScrollBar->setFixedHeight(511);
            } else {
                mainScrollBar->setFixedHeight(493);
            }
            artistScrollBar->setFixedHeight(511);

        }
    }
    void windowMinimized(){
        mainScrollBar->move(792,0);
        if(mainTable->currentContext==ALL){
            mainScrollBar->setFixedHeight(511);
        } else {
            mainScrollBar->setFixedHeight(493);
        }
        artistScrollBar->setFixedHeight(511);
    }
};

#endif
