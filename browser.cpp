#include "browser.h"

Browser::Browser(QWidget *parent) : QWidget(parent){
    searchHolder = new QHBoxLayout;
    searchBar_ = new QLineEdit;
    browserLayout = new QVBoxLayout;
    artists = new ArtistTable;
    playlists = new PlaylistTable;
    artistTableHeader = new QTableWidget;
    artistTableHeader->horizontalHeader()->setStyleSheet("QHeaderView::section{"
                                                         "spacing: 0px;"
                                                         "background-color:black;"
                                                         "color:rgba(229, 243, 255);"
                                                         "border-width:0px;"
                                                         "padding:4px;"
                                                         "font-size:14;"
                                                         "font-style:Avenir;"
                                                         "}");
    QStringList artistHeaderLabel;
    artistHeaderLabel << tr("Artists");
    artistTableHeader->setColumnCount(1);
    artistTableHeader->setHorizontalHeaderLabels(artistHeaderLabel);
    artistTableHeader->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
    artistTableHeader->verticalHeader()->setResizeMode(QHeaderView::Stretch);
    artistTableHeader->setFixedHeight(20);
    searchBar_->setPlaceholderText(QString("Search Library..."));
    searchBar_->setStyleSheet("QLineEdit{border: 1px solid gray;"
                              "border-radius: 10px;"
                              "background-color:rgba(51, 65, 76);"
                              "color:rgba(229, 243, 255);"
                              "}");
    searchBar_->setTextMargins(4,0,4,0);
    searchBar_->setLayout(searchHolder);
    //searchHolder->addWidget(searchBar_);
    browserLayout->setSpacing(0);
    browserLayout->setContentsMargins(0,0,0,0);
    //browserLayout->addWidget(searchBar_);
    browserLayout->addWidget(artistTableHeader);
    browserLayout->addWidget(artists);
    //browserLayout->addWidget(playlists);
    setLayout(browserLayout);
}

ArtistTable* Browser::artistTable(){
    return artists;
}

PlaylistTable* Browser::playlistTable(){
    return playlists;
}

QLineEdit* Browser::searchBar(){
    return searchBar_;
}
