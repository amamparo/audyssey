#include "artisttable.h"

ArtistTable::ArtistTable(QWidget *parent) : QTableWidget(parent){
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setColumnCount(3);
    horizontalHeader()->hide();
    horizontalHeader()->setResizeMode(QHeaderView::Stretch);
    horizontalHeader()->setResizeMode(2,QHeaderView::Fixed);
    setSelectionMode(QAbstractItemView::ExtendedSelection);
    setSelectionBehavior(QAbstractItemView::SelectRows);
    hideColumn(1);
    verticalHeader()->hide();
    setColumnWidth(2,20);
    setFixedWidth(220);
    setShowGrid(false);
    horizontalHeader()->setHighlightSections(false);
    setStyleSheet("QTableWidget{"
                  "color:rgba(229, 229, 229);"
                  "background-color:rgba(76, 88, 102);"
                  "border:0px solid rgba(51, 51, 51);"
                  "border-left-width:1px;"
                  "border-top-width:1px;"
                  "}"
                  "QTableWidget::item:selected{ background-color: rgba(153, 166, 178);}");
    QFont newFont("Avenir", 14);
    newFont.setWeight(QFont::Light);
    setFont(newFont);
}

void ArtistTable::populate(){
    setRowCount(0);
    //first make sure xml library exists
    if(!fileExists(getXmlPath())){
        //create xml file & do nothing
        rapidxml::xml_document<> doc;
        rapidxml::xml_node<>* decl = doc.allocate_node(rapidxml::node_declaration);
        decl->append_attribute(doc.allocate_attribute("version", "1.0"));
        decl->append_attribute(doc.allocate_attribute("encoding", "utf-8"));
        doc.append_node(decl);
        rapidxml::xml_node<>* root = doc.allocate_node(rapidxml::node_element, "library");
        root->append_attribute(doc.allocate_attribute("version", "1.0"));
        doc.append_node(root);
        ofstream file_stored(getXmlPath().c_str());
        file_stored << doc;
        file_stored.close();
    } else {
        //read from xml doc
        rapidxml::xml_document<> doc;
        ifstream file(getXmlPath().c_str());
        stringstream buffer;
        buffer << file.rdbuf();
        string content(buffer.str());
        doc.parse<0>(&content[0]);
        rapidxml::xml_node<>* root = doc.first_node("library");
        rapidxml::xml_node<>* artist_node = root->first_node("artist");
        for(; artist_node; artist_node = artist_node->next_sibling()){
            string artist = artist_node->first_attribute("id")->value();
            if(strncmp(artist.c_str(),"",sizeof(artist))){
                QTableWidgetItem *artist_item = new QTableWidgetItem(QString(artist_node->first_attribute("id")->value()));
                artist_item->setFlags(artist_item->flags() ^ Qt::ItemIsEditable);
                int currentRow = rowCount();
                insertRow(currentRow);
                setItem(currentRow, 0, artist_item);
                QTableWidgetItem *type = new QTableWidgetItem(1);
                type->setData(Qt::DisplayRole, 1);
                setItem(currentRow,1,type);
                verticalHeader()->resizeSection(currentRow, 24);
                QTableWidgetItem *empty = new QTableWidgetItem;
                setItem(currentRow,2,empty);
            }
        }
        int currentRow = rowCount();
        insertRow(currentRow);
        QTableWidgetItem *empty = new QTableWidgetItem;
        setItem(currentRow, 2, empty);
        QTableWidgetItem *allArtists = new QTableWidgetItem(QString("All Artists"));
        allArtists->setFlags(allArtists->flags() ^ Qt::ItemIsEditable);
        QTableWidgetItem *type = new QTableWidgetItem(0);
        type->setData(Qt::DisplayRole, 0);
        setItem(currentRow,0,allArtists);
        setItem(currentRow,1,type);
        verticalHeader()->resizeSection(currentRow, 24);

        sortItems(0);
        sortItems(1);
        doc.clear();
        int count = rowCount();
        for(int i=0; i<count; i++){
            if(i%2==0){
                item(i,0)->setBackgroundColor(QColor(102, 113, 127));
                item(i,2)->setBackgroundColor(QColor(102, 113, 127));
            } else {
                item(i,0)->setBackgroundColor(QColor(76, 88, 102));
                item(i,2)->setBackgroundColor(QColor(76, 88, 102));
            }
        }
    }
}

void ArtistTable::setNowPlayingIcon(int context, string artist){
    clearNowPlayingIcon();
    if(context==ALL){
        QTableWidgetItem *nowPlaying = new QTableWidgetItem;
        nowPlaying->setIcon(QIcon(QPixmap(":playback/icons/speaker_icon.png").scaled(17,17,Qt::KeepAspectRatio)));
        setItem(0,2,nowPlaying);
        item(0,2)->setBackgroundColor(item(0,0)->backgroundColor());
    } else if(context == ARTIST){
        int count = rowCount();
        for(int i=1; i<count; i++){
            string this_artist = item(i,0)->text().toStdString();
            if(!strcasecmp(this_artist.c_str(),artist.c_str())){
                QTableWidgetItem *nowPlaying = new QTableWidgetItem;
                nowPlaying->setIcon(QIcon(QPixmap(":playback/icons/speaker_icon.png").scaled(17,17,Qt::KeepAspectRatio)));
                setItem(i,2,nowPlaying);
                item(i,2)->setBackgroundColor(item(i,0)->backgroundColor());
                break;
            }
        }
    }
}

void ArtistTable::clearNowPlayingIcon(){
    int count = rowCount();
    for(int i=0; i<count; i++){
        QTableWidgetItem *empty = new QTableWidgetItem;
        setItem(i,2,empty);
        item(i,2)->setBackgroundColor(item(i,0)->backgroundColor());
    }
}
