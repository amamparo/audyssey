#-------------------------------------------------
#
# Project created by QtCreator 2013-04-17T02:56:22
#
#-------------------------------------------------

QT       += core gui
QT       += phonon

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Audyssey
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    maintable.cpp \
    artisttable.cpp \
    playlisttable.cpp \
    browser.cpp

HEADERS  += mainwindow.h \
    maintable.h \
    artisttable.h \
    playlisttable.h \
    browser.h \
    util.h \
    fileref.h \
    tag.h \
    titlebar.h

INCLUDEPATH += /usr/include/taglib
LIBS += -L/usr/lib/taglib -ltag

ICON = speaker.icns

OTHER_FILES += \
    Notes.txt

RESOURCES += \
    icons.qrc

FORMS += \
    mainwindow.ui


