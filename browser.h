#ifndef BROWSER_H
#define BROWSER_H

#include <QtGui>
#include <QWidget>
#include "artisttable.h"
#include "playlisttable.h"

class Browser : public QWidget
{
    Q_OBJECT
public:
    explicit Browser(QWidget *parent = 0);

    //get methods
    ArtistTable* artistTable();
    PlaylistTable* playlistTable();
    QLineEdit* searchBar();

private:
    QTableWidget *artistTableHeader;
    QBoxLayout *searchHolder;
    QLineEdit *searchBar_;
    QBoxLayout *browserLayout;
    ArtistTable *artists;
    PlaylistTable *playlists;
};

#endif // BROWSER_H
