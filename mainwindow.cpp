#include <QtGui>
#include "mainwindow.h"

MainWindow::MainWindow(){
    m_mouse_down = false;
    setFrameShape(Panel);
    setWindowFlags(Qt::FramelessWindowHint);

    /* instantiate media-related elements */
    audioOutput = new Phonon::AudioOutput(Phonon::MusicCategory, this);
    mediaObject = new Phonon::MediaObject(this);
    seekSlider = new Phonon::SeekSlider(this);
    seekSlider->setMediaObject(mediaObject);
    connect(mediaObject, SIGNAL(stateChanged(Phonon::State,Phonon::State)),this, SLOT(stateChanged(Phonon::State,Phonon::State)));
    connect(mediaObject, SIGNAL(finished()), this, SLOT(mediaObjectFinished()));
    Phonon::createPath(mediaObject, audioOutput);

    setMinimumSize(1024,600);
    setupActions();
    setupUi();
    mainTable->currentContext = ALL;
    mainTable->populate();
    artistTable->populate();
}

/***********************************************
 * setup the different elements of the main UI *
 ***********************************************/
void MainWindow::setupUi(){
    //instantiate shit
    QVBoxLayout *mainLayout = new QVBoxLayout(this);  //contains everything in UI
    QBoxLayout *mainSection = new QHBoxLayout;   //middle section (largest)
    QBoxLayout *mainSectionLeft = new QVBoxLayout;
    QBoxLayout *mainSectionRight = new QVBoxLayout;
    QToolBar *bottomSection = new QToolBar;
    QTableWidget *mainTableHeader = new QTableWidget;
    mainTable = new MainTable;
    mainScrollBar = new QScrollArea;
    QTableWidget *artistTableHeader = new QTableWidget;
    artistTable = new ArtistTable;
    contextLabel = new QLabel;
    titleBar = new TitleBar(this);
    /*****setup left side of main section*****/
    //context label (displays info about the current context of the main table)
    contextLabel->setAlignment(Qt::AlignHCenter);
    contextLabel->setFixedHeight(17);
    contextLabel->hide();

    QFont avenir("Avenir", 14);
    avenir.setWeight(QFont::Light);
    setFont(avenir);

    //create QScrollArea to hover over main table & replace its scrollbar
    mainScrollBar->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    mainScrollBar->viewport()->setVisible(false);
    mainScrollBar->setParent(mainTable);
    mainScrollBar->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    mainScrollBar->setFixedHeight(511);
    mainScrollBar->move(792,0);
    mainScrollBar->setMouseTracking(false);
    mainScrollBar->viewport()->setMouseTracking(false);
    mainScrollBar->verticalScrollBar()->setRange(mainTable->verticalScrollBar()->minimum(),mainTable->verticalScrollBar()->maximum());
    connect(mainScrollBar->verticalScrollBar(), SIGNAL(valueChanged(int)), mainTable->verticalScrollBar(), SLOT(setValue(int)));
    connect(mainTable->verticalScrollBar(), SIGNAL(valueChanged(int)), mainScrollBar->verticalScrollBar(), SLOT(setValue(int)));
    connect(mainTable->verticalScrollBar(), SIGNAL(rangeChanged(int,int)),this,SLOT(updateMainScrollBarRange(int,int)));
    mainScrollBar->setFixedWidth(10);
    connect(titleBar->maximize, SIGNAL(clicked()), this, SLOT(windowMaximized()));
    connect(titleBar->minimize, SIGNAL(clicked()), this, SLOT(windowMinimized()));

    QStringList mainHeaderLabels;
    mainHeaderLabels<<tr("Title")<<tr("Time")<<tr("Artist")<<tr("Album")<<tr("Track")<<tr("");
    mainTableHeader->setColumnCount(5);
    mainTableHeader->setHorizontalHeaderLabels(mainHeaderLabels);
    mainTableHeader->horizontalHeader()->setResizeMode(QHeaderView::Fixed);
    mainTableHeader->horizontalHeader()->setResizeMode(0,QHeaderView::Stretch);
    mainTableHeader->verticalHeader()->setResizeMode(QHeaderView::Stretch);
    mainTableHeader->setShowGrid(false);
    mainTableHeader->verticalHeader()->hide();
    mainTableHeader->setFixedHeight(22);
    mainTableHeader->setColumnWidth(1,60);
    mainTableHeader->setColumnWidth(2,210);
    mainTableHeader->setColumnWidth(3,210);
    mainTableHeader->setColumnWidth(4,50);

    //put together layout left section of main part of UI
    mainSectionLeft->addWidget(mainTableHeader);
    mainSectionLeft->addWidget(mainTable);
    mainSectionLeft->addWidget(contextLabel);

    //setup right side of main section
    //QScrollArea to hover over artistTable & replace its scrollbar
    artistScrollBar = new QScrollArea;
    artistScrollBar->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    artistScrollBar->viewport()->setVisible(false);
    artistScrollBar->setParent(artistTable);
    artistScrollBar->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    artistScrollBar->setFixedHeight(511);
    artistScrollBar->move(208,0);
    artistScrollBar->setMouseTracking(false);
    artistScrollBar->viewport()->setMouseTracking(false);
    artistScrollBar->verticalScrollBar()->setRange(artistTable->verticalScrollBar()->minimum(),artistTable->verticalScrollBar()->maximum());
    connect(artistScrollBar->verticalScrollBar(), SIGNAL(valueChanged(int)), artistTable->verticalScrollBar(), SLOT(setValue(int)));
    connect(artistTable->verticalScrollBar(), SIGNAL(valueChanged(int)), artistScrollBar->verticalScrollBar(), SLOT(setValue(int)));
    connect(artistTable->verticalScrollBar(), SIGNAL(rangeChanged(int,int)),this,SLOT(updateArtistScrollBarRange(int,int)));
    artistScrollBar->setFixedWidth(10);

    QStringList artistHeaderLabel;
    artistHeaderLabel << tr("Artists");
    artistTableHeader->setColumnCount(1);
    artistTableHeader->setHorizontalHeaderLabels(artistHeaderLabel);
    artistTableHeader->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
    artistTableHeader->verticalHeader()->setResizeMode(QHeaderView::Stretch);
    artistTableHeader->setShowGrid(false);
    artistTableHeader->verticalHeader()->hide();
    artistTableHeader->setFixedHeight(22);
    artistTableHeader->setFixedWidth(220);

    //put together right side of main section
    mainSectionRight->addWidget(artistTableHeader);
    mainSectionRight->addWidget(artistTable);

    //join all parts of mainSection
    mainSection->addLayout(mainSectionLeft);
    mainSection->addLayout(mainSectionRight);
    //mainSection->setStretch(0,4);
    connect(mainTable, SIGNAL(cellDoubleClicked(int,int)),this,SLOT(mainTableDoubleClicked(int ,int)));
    connect(mainTable, SIGNAL(cellClicked(int,int)),this,SLOT(mainTableClicked(int,int)));
    connect(artistTable, SIGNAL(cellClicked(int,int)), this, SLOT(artistTableClicked(int,int)));

    //setup bottom section
    QLabel *spacer1 = new QLabel;
    spacer1->setFixedWidth(10);
    QLabel *spacer2 = new QLabel;
    spacer2->setFixedWidth(10);
    bottomSection->addAction(prevAction);
    bottomSection->addAction(playAction);
    bottomSection->addAction(nextAction);
    bottomSection->addWidget(seekSlider);
    bottomSection->addWidget(spacer1);
    bottomSection->setFixedHeight(45);
    bottomSection->addWidget(spacer2);
    mainLayout->addWidget(titleBar);
    mainLayout->addLayout(mainSection);
    mainLayout->addWidget(bottomSection);
    mainLayout->setContentsMargins(0,0,0,0);
    mainLayout->setMargin(0);
    mainLayout->setSpacing(0);

    //set stylesheets
    setStyleSheet("QFrame{"
                  "border:0px solid black;" //get rid of main window's border
                  "}");
    contextLabel->setStyleSheet("QLabel{"
                                "background-color:rgba(183, 196, 208);"
                                "color:rgba(51,51,51);"
                                "}");
    mainScrollBar->verticalScrollBar()->setStyleSheet("QScrollBar:vertical {"
                                               "background: transparent;"
                                               "width:10px;"
                                               "margin-top:3px;"
                                               "margin-bottom:3px;"
                                               "}"
                                               "QScrollBar::handle:vertical {"
                                               "background-color:rgba(66, 66, 66, 75%);"
                                               "border:1px solid rgba(66, 66, 66, 75%);"
                                               "style:outset;"
                                               "border-radius:5px;"
                                               "min-height: 20px;"
                                               "max-height: 600px;"
                                               "}"
                                               "QScrollBar::add-line:vertical {"
                                               "border: 0px solid grey;"
                                               "background: transparent;"
                                               "height: 0px;"
                                               "subcontrol-position: bottom;"
                                               "subcontrol-origin: margin;"
                                               "}"
                                               "QScrollBar::sub-line:vertical {"
                                               "border: 0px solid grey;"
                                               "background: transparent;"
                                               "height: 0px;"
                                               "subcontrol-position: top;"
                                               "subcontrol-origin: margin;"
                                               "}"
                                               "QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {"
                                               "background: none;"
                                               "}");
    mainTableHeader->horizontalHeader()->setStyleSheet("QHeaderView::section{"
                                                       "spacing: 0px;"
                                                       "background-color:qlineargradient(spread:reflect, x1:0.506926,"
                                                       "y1:0.501, x2:0.503, y2:0.926182,stop:0.344828 rgba(85, 85, 85, 255),"
                                                       "stop:0.970443 rgba(66, 66, 66, 255));"
                                                       "color:rgba(204, 204, 204);"
                                                       "border:0px solid rgba(51, 51, 51);"
                                                       "border-left-width:1px;"
                                                       "border-top-width:1px;"
                                                       "padding:4px;"
                                                       "font-size:14;"
                                                       "font-weight:700;"
                                                       "}");
    mainTableHeader->setStyleSheet("QTableWidget{"
                                   "color:rgba(229, 229, 229);"
                                   "background-color:rgba(76, 88, 102);"
                                   "border:0px;"
                                   "}"
                                   "QTableWidget::item:selected{ background-color: rgba(153, 166, 178);}");
    artistScrollBar->verticalScrollBar()->setStyleSheet("QScrollBar:vertical {"
                                               "background: transparent;"
                                               "width:10px;"
                                               "margin-top:3px;"
                                               "margin-bottom:3px;"
                                               "}"
                                               "QScrollBar::handle:vertical {"
                                               "background-color:rgba(66, 66, 66, 75%);"
                                               "border:1px solid rgba(66, 66, 66, 75%);"
                                               "style:outset;"
                                               "border-radius:5px;"
                                               "min-height: 20px;"
                                               "max-height: 600px;"
                                               "}"
                                               "QScrollBar::add-line:vertical {"
                                               "border: 0px solid grey;"
                                               "background: transparent;"
                                               "height: 0px;"
                                               "subcontrol-position: bottom;"
                                               "subcontrol-origin: margin;"
                                               "}"
                                               "QScrollBar::sub-line:vertical {"
                                               "border: 0px solid grey;"
                                               "background: transparent;"
                                               "height: 0px;"
                                               "subcontrol-position: top;"
                                               "subcontrol-origin: margin;"
                                               "}"
                                               "QScrollBar::add-page:vertical, QScrollBar::sub-page:vertical {"
                                               "background: none;"
                                               "}");
    artistTableHeader->horizontalHeader()->setStyleSheet("QHeaderView::section{"
                                                         "spacing: 0px;"
                                                         "background-color:qlineargradient(spread:reflect, x1:0.506926,"
                                                         "y1:0.501, x2:0.503, y2:0.926182,stop:0.344828 rgba(85, 85, 85, 255),"
                                                         "stop:0.970443 rgba(66, 66, 66, 255));"
                                                         "color:rgba(204, 204, 204);"
                                                         "border:0px solid rgba(51, 51, 51);"
                                                         "border-left-width:1px;"
                                                         "border-top-width:1px;"
                                                         "padding:4px;"
                                                         "font-size:14;"
                                                         "font-weight:700;"
                                                         "}");
    artistTableHeader->setStyleSheet("QTableWidget{"
                                     "color:rgba(229, 229, 229);"
                                     "background-color:rgba(76, 88, 102);"
                                     "border:0px;"
                                     "}"
                                     "QTableWidget::item:selected{ background-color: rgba(153, 166, 178);}");
    bottomSection->setStyleSheet("QToolBar{"
                                 "background-color:qlineargradient(spread:pad, x1:0.516778, y1:0.08, x2:0.518, y2:0.983,"
                                 "stop:0.241379 rgba(94, 94, 94, 255), stop:0.551724 rgba(66, 66, 66, 255));"
                                 "border:0px solid rgba(51, 51, 51);"
                                 "border-top-width:1px;"
                                 "spacing:0px;"
                                 "}");
}

/*******************************************
 * instantiate actions & setup connections *
 *******************************************/
void MainWindow::setupActions(){
    addFolderAction = new QAction(tr("Add &Files"), this);
    addFolderAction->setShortcut(tr("Ctrl+F"));
    exitAction = new QAction(tr("E&xit"), this);
    exitAction->setShortcuts(QKeySequence::Quit);
    aboutAction = new QAction(tr("A&bout"), this);
    aboutAction->setShortcut(tr("Ctrl+B"));\
    playAction = new QAction(style()->standardIcon(QStyle::SP_MediaPlay), tr("Play"), this);
    playAction->setShortcut(tr("Ctrl+P"));
    playAction->setDisabled(true);
    playAction->setIcon(QIcon(QPixmap(":/playback/icons/MCPlay.png")));
    prevAction = new QAction(style()->standardIcon(QStyle::SP_MediaSeekBackward), tr("Previous"), this);
    prevAction->setDisabled(true);
    prevAction->setIcon(QIcon(QPixmap(":/playback/icons/MCPrev.png")));
    nextAction = new QAction(style()->standardIcon(QStyle::SP_MediaSeekForward), tr("Next"), this);
    nextAction->setDisabled(true);
    nextAction->setIcon(QIcon(QPixmap(":/playback/icons/MCNext.png")));

    connect(playAction, SIGNAL(triggered()), this, SLOT(play_pause()));
    connect(addFolderAction, SIGNAL(triggered()), this, SLOT(addFolder()));
    connect(exitAction, SIGNAL(triggered()), this, SLOT(close()));
    connect(aboutAction, SIGNAL(triggered()), this, SLOT(about()));
}

/**********************************************************
 * add song with given filepath to library (write to xml) *
 **********************************************************/
void MainWindow::importSong(string path){
    TagLib::FileRef f(path.c_str());
    TagLib::Tag *tag = f.tag();
    if(!f.isNull() && f.tag() && f.audioProperties()){
        string title, time, artist, album, track, location;
        location = path.c_str();
        title = tag->title().toCString();
        artist = tag->artist().toCString();
        album = tag->album().toCString();
        track = tag->track();

        //setup `time`
        TagLib::AudioProperties *properties = f.audioProperties();
        int seconds = properties->length() % 60;
        int minutes = (properties->length() - seconds) / 60;
        char secondsString[3];
        sprintf(secondsString, "%02i", seconds);
        stringstream ss;
        ss << minutes << ":" << secondsString;
        time = ss.str();

        if(!fileExists(getXmlPath())){
            //create xml file & add song
            rapidxml::xml_document<> doc;
            rapidxml::xml_node<>* decl = doc.allocate_node(rapidxml::node_declaration);
            decl->append_attribute(doc.allocate_attribute("version", "1.0"));
            decl->append_attribute(doc.allocate_attribute("encoding", "utf-8"));
            doc.append_node(decl);
            rapidxml::xml_node<>* root = doc.allocate_node(rapidxml::node_element, "library");
            root->append_attribute(doc.allocate_attribute("version", "1.0"));
            doc.append_node(root);
            rapidxml::xml_node<>* artist_node = doc.allocate_node(rapidxml::node_element, "artist");
            artist_node->append_attribute(doc.allocate_attribute("id", artist.c_str()));
            root->append_node(artist_node);
            rapidxml::xml_node<>* album_node = doc.allocate_node(rapidxml::node_element, "album");
            album_node->append_attribute(doc.allocate_attribute("id", album.c_str()));
            artist_node->append_node(album_node);
            rapidxml::xml_node<>* song_node = doc.allocate_node(rapidxml::node_element, "song");
            song_node->append_attribute(doc.allocate_attribute("title", title.c_str()));
            song_node->append_attribute(doc.allocate_attribute("track", track.c_str()));
            song_node->append_attribute(doc.allocate_attribute("time", time.c_str()));
            song_node->append_attribute(doc.allocate_attribute("location", location.c_str()));
            album_node->append_node(song_node);
            ofstream file_stored(getXmlPath().c_str());
            file_stored << doc;
            file_stored.close();
        } else {
            rapidxml::xml_document<> doc;
            ifstream file(getXmlPath().c_str());
            stringstream buffer;
            buffer << file.rdbuf();
            file.close();
            string content(buffer.str());
            doc.parse<0>(&content[0]);
            rapidxml::xml_node<>* root = doc.first_node("library");
            rapidxml::xml_node<>* artist_node = root->first_node("artist");
            for(; artist_node; artist_node = artist_node->next_sibling()){
                if(strcasecmp(artist_node->first_attribute("id")->value(), artist.c_str())==0){
                    break;
                }
            }
            if(artist_node){
                //found artist node
                rapidxml::xml_node<>* album_node = artist_node->first_node("album");
                for(; album_node; album_node = album_node->next_sibling()){
                    if(strcasecmp(album_node->first_attribute("id")->value(), album.c_str())==0){
                        break;
                    }
                }
                if(album_node){
                    //found album node
                    rapidxml::xml_node<>* song_node = album_node->first_node("song");
                    for(; song_node; song_node = song_node->next_sibling()){
                        if(strcasecmp(song_node->first_attribute("location")->value(), location.c_str())==0){
                            break;
                        }
                    }
                    if(!song_node){
                        rapidxml::xml_node<>* new_song_node = doc.allocate_node(rapidxml::node_element, "song");
                        new_song_node->append_attribute(doc.allocate_attribute("title", title.c_str()));
                        new_song_node->append_attribute(doc.allocate_attribute("track", track.c_str()));
                        new_song_node->append_attribute(doc.allocate_attribute("time", time.c_str()));
                        new_song_node->append_attribute(doc.allocate_attribute("location", location.c_str()));
                        album_node->append_node(new_song_node);
                    }
                } else {
                    //create album node
                    rapidxml::xml_node<>* new_album_node = doc.allocate_node(rapidxml::node_element, "album");
                    new_album_node->append_attribute(doc.allocate_attribute("id", album.c_str()));
                    rapidxml::xml_node<>* new_song_node = doc.allocate_node(rapidxml::node_element, "song");
                    new_song_node->append_attribute(doc.allocate_attribute("title", title.c_str()));
                    new_song_node->append_attribute(doc.allocate_attribute("track", track.c_str()));
                    new_song_node->append_attribute(doc.allocate_attribute("time", time.c_str()));
                    new_song_node->append_attribute(doc.allocate_attribute("location", location.c_str()));
                    new_album_node->append_node(new_song_node);
                    artist_node->append_node(new_album_node);
                }
            } else {
                //create new artist node
                rapidxml::xml_node<>* new_artist_node = doc.allocate_node(rapidxml::node_element, "artist");
                new_artist_node->append_attribute(doc.allocate_attribute("id", artist.c_str()));
                rapidxml::xml_node<>* new_album_node = doc.allocate_node(rapidxml::node_element, "album");
                new_album_node->append_attribute(doc.allocate_attribute("id", album.c_str()));
                rapidxml::xml_node<>* new_song_node = doc.allocate_node(rapidxml::node_element, "song");
                new_song_node->append_attribute(doc.allocate_attribute("title", title.c_str()));
                new_song_node->append_attribute(doc.allocate_attribute("track", track.c_str()));
                new_song_node->append_attribute(doc.allocate_attribute("time", time.c_str()));
                new_song_node->append_attribute(doc.allocate_attribute("location", location.c_str()));
                new_album_node->append_node(new_song_node);
                new_artist_node->append_node(new_album_node);
                root->append_node(new_artist_node);
            }
            ofstream file_stored(getXmlPath().c_str());
            file_stored << doc;
            file_stored.close();
        }
    }
}

/******************************************************
 * update window title to current song artist & title *
 ******************************************************/
void MainWindow::changeNowPlayingLabel(){
    Phonon::MediaSource newSource = mediaObject->currentSource();
    TagLib::FileRef f(newSource.fileName().toStdString().c_str());
    TagLib::Tag *tag = f.tag();
    QString song, artist;
    if(strcasecmp(f.tag()->title().toCString(),"")==0){
        song = QString("(no title)");
    } else {
        song = QString(tag->title().toCString());
    }
    if(strcasecmp(f.tag()->artist().toCString(),"")==0){
        artist = QString("(no artist)");
    } else {
        artist = QString(tag->artist().toCString());
    }
    stringstream ss;
    ss << artist.toStdString().c_str() << " - " << song.toStdString().c_str();
    setWindowTitle(QString(ss.str().c_str()));
    titleBar->topLabel->setText(QString(ss.str().c_str()));
}

/*************************************************
 * display info about program (self-explanatory) *
 *************************************************/
void MainWindow::about(){
    QMessageBox::information(this, tr("About Audyssey"), tr("Programmed by Aaron Mamparo (amampa2@uic.edu)"));
}

void MainWindow::mainTableClicked(int, int){
    artistTable->clearSelection();
}

/***********************************************************************
 * start music playback on item that's doubleclicked within mainTable *
 ***********************************************************************/
void MainWindow::mainTableDoubleClicked(int row, int){
    mainTable->clearNowPlayingIcon();
    mainTable->nowPlayingContext = mainTable->currentContext;
    QString path = mainTable->item(row,6)->text();
    Phonon::MediaSource newSource(path);
    mediaObject->setCurrentSource(newSource);
    mediaObject->play();
    playAction->setIcon(QIcon(QPixmap(":/playback/icons/MCPause.png")));
    playAction->setEnabled(true);
}

/*****************
 * private slots *
 *****************/

/*handle the play/pause button*/
void MainWindow::play_pause(){
    if(mediaObject->state()==Phonon::PlayingState){
        mediaObject->pause();
    } else {
        mediaObject->play();
    }
}

/*prompt the user to to select a folder to add to library*/
void MainWindow::addFolder(){
    QString directory = QFileDialog::getExistingDirectory(this, tr("Where's your music?"));
    QDir dir(directory);
    QStack<QString> stack;
    stack.push(dir.absolutePath());
    while (!stack.isEmpty()) {
        QString sSubdir = stack.pop();
        QDir subdir(sSubdir);
        QStringList filter;
        filter << "*.mp3" << "*.m4a";
        QFileInfoList entries = subdir.entryInfoList(filter, QDir::Files);
        for(int i=0; i<entries.size(); i++){
            importSong(entries.at(i).absoluteFilePath().toStdString());
        }
        QFileInfoList infoEntries = subdir.entryInfoList(QStringList(), QDir::AllDirs | QDir::NoDotAndDotDot);
        for(int i=0; i<infoEntries.size(); i++) {
            QFileInfo& item = infoEntries[i];
            stack.push(item.absoluteFilePath());
        }
    }
    mainTable->populate();
    artistTable->populate();
}

/* handle change of mediaObject state */
void MainWindow::stateChanged(Phonon::State newState, Phonon::State /*oldState*/){
    switch (newState) {
        case Phonon::ErrorState:
            if (mediaObject->errorType() == Phonon::FatalError) {
                QMessageBox::warning(this, tr("Fatal Error"), mediaObject->errorString());
            } else {
                QMessageBox::warning(this, tr("Error"), mediaObject->errorString());
            }
            break;
        case Phonon::PlayingState:{
            string cur_source = mediaObject->currentSource().fileName().toStdString();
            mainTable->setNowPlayingIcon(cur_source);
            changeNowPlayingLabel();
            mainTable->nowPlayingSource = mediaObject->currentSource().fileName();
            //also set NP icon for artist table
            TagLib::FileRef f(mainTable->nowPlayingSource.toStdString().c_str());
            TagLib::Tag *tag = f.tag();
            string artist = tag->artist().toCString();
            artistTable->setNowPlayingIcon(mainTable->nowPlayingContext, artist);
            playAction->setEnabled(true);
            playAction->setIcon(QIcon(QPixmap(":/playback/icons/MCPause.png")));
            break;
        }
        case Phonon::StoppedState:
            playAction->setEnabled(false);
            titleBar->topLabel->setText(QString("Audyssey"));
            break;
        case Phonon::PausedState:
            playAction->setIcon(QIcon(QPixmap(":/playback/icons/MCPlay.png")));
            break;
        case Phonon::BufferingState:
            break;
        default:
            ;
    }
}

/*************************************************************
 * take text from search bar & filter main table accordingly *
 *************************************************************/
/*void MainWindow::search(){
    string query = browser->searchBar()->text().toStdString();
    mainTable->currentContext = ALL;
    if(!strcasecmp(query.c_str()," ")){
        browser->searchBar()->clear();
        return;
    }
    mainTable->filter(QString(query.c_str()));
    if(strcasecmp(query.c_str(),"")){
        stringstream context;
        context << "Showing search results for \"" << query.c_str() << "\"";
        contextLabel->setText(QString(context.str().c_str()));
        contextLabel->show();
    } else {
        contextLabel->hide();
        browser->searchBar()->clearFocus();
    }
}

void MainWindow::searchReturnPressed(){
    string query = browser->searchBar()->text().toStdString();
    mainTable->filter(QString(query.c_str()));
    browser->searchBar()->clearFocus();
    browser->searchBar()->clear();
}*/

/********************************************************************************
 * show only songs by given artist when user makes selection from artists table *
 ********************************************************************************/
void MainWindow::artistTableClicked(int row, int){
    int type = artistTable->item(row,1)->type();
    if(type==1){
        mainTable->currentContext = ARTIST;
        if(isFullScreen()){
            mainScrollBar->setFixedHeight(QApplication::desktop()->height()-106);
        } else {
            mainScrollBar->setFixedHeight(493);
        }
        string artist = artistTable->item(row,0)->text().toStdString();
        mainTable->filterByArtist(QString(artist.c_str()));
        stringstream context;
        context << "Showing songs by \"" << artist.c_str() << "\"";
        contextLabel->setText(QString(context.str().c_str()));
        contextLabel->show();
    } else {
        mainTable->currentContext = ALL;
        if(isFullScreen()){
            mainScrollBar->setFixedHeight(QApplication::desktop()->height()-89);
        } else {
            mainScrollBar->setFixedHeight(511);
        }
        mainTable->populate();
        contextLabel->hide();
    }
}

/*********************
 * show/hide browser *
 *********************/
/*void MainWindow::showBrowser(){
    if(browser->isHidden()){
        showBrowserButton->setText(QString("Hide Browser"));
        browser->show();
    } else {
        showBrowserButton->setText(QString("Show Browser"));
        browser->hide();
    }
}*/

void MainWindow::mediaObjectFinished(){
    mainTable->clearNowPlayingIcon();
    artistTable->clearNowPlayingIcon();
    titleBar->topLabel->setText(QString("Audyssey"));
    playAction->setEnabled(false);
    playAction->setIcon(QIcon(QPixmap(":/playback/icons/MCPlay.png")));
}
