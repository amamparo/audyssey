#include "playlisttable.h"

PlaylistTable::PlaylistTable(QWidget *parent) : QTableWidget(parent){
    setColumnCount(1);
    QStringList label;
    label << QString("Playlists");
    setHorizontalHeaderLabels(label);
    horizontalHeader()->setResizeMode(QHeaderView::Stretch);
    verticalHeader()->hide();
}
