#include "maintable.h"

MainTable::MainTable(QWidget *parent) : QTableWidget(parent){
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setColumnCount(8);
    QStringList headers;
    headers<<tr("")<<tr("Title")<<tr("Time")<<tr("Artist")<<tr("Album")<<tr("Track")<<tr("Location");
    setHorizontalHeaderLabels(headers);
    setSelectionMode(QAbstractItemView::ExtendedSelection);
    setSelectionBehavior(QAbstractItemView::SelectRows);
    setColumnHidden(6,true);
    horizontalHeader()->setResizeMode(QHeaderView::Fixed);
    horizontalHeader()->setResizeMode(1,QHeaderView::Stretch);
    verticalHeader()->hide();
    horizontalHeader()->hide();
    horizontalHeader()->setHighlightSections(false);
    setShowGrid(false);
    setColumnWidth(0,20);
    setColumnWidth(2,60);
    setColumnWidth(3,210);
    setColumnWidth(4,210);
    setColumnWidth(5,40);
    setColumnWidth(7,10);
    setAlternatingRowColors(true);
    horizontalHeader()->setStyleSheet("QHeaderView::section{"
                                      "spacing: 0px;"
                                      "background-color:qlineargradient(spread:reflect, x1:0.506926,"
                                      "y1:0.501, x2:0.503, y2:0.926182,stop:0.344828 rgba(85, 85, 85, 255),"
                                      "stop:0.970443 rgba(66, 66, 66, 255));"
                                      "color:rgba(204, 204, 204);"
                                      "border:0px;"
                                      "padding:4px;"
                                      "font-size:14;"
                                      "font-weight:700;"
                                      "}");
    setStyleSheet("QTableWidget{"
                  "color:rgba(229, 229, 229);"
                  "background-color:rgba(76, 88, 102);"
                  "border:0px solid rgba(51,51,51);"
                  "border-top-width:1px;"
                  "}"
                  "QTableWidget::item:selected{ background-color: rgba(153, 166, 178);}");
    QFont newFont("Avenir", 14);
    newFont.setWeight(QFont::Light);
    setFont(newFont);
    QPalette p = this->palette();
    p.setColor(QPalette::AlternateBase, QColor(102,113,127));
    this->setPalette(p);

}

void MainTable::populate(){
    setRowCount(0);
    //first make sure xml library exists
    if(!fileExists(getXmlPath())){
        //create xml file & do nothing
        rapidxml::xml_document<> doc;
        rapidxml::xml_node<>* decl = doc.allocate_node(rapidxml::node_declaration);
        decl->append_attribute(doc.allocate_attribute("version", "1.0"));
        decl->append_attribute(doc.allocate_attribute("encoding", "utf-8"));
        doc.append_node(decl);
        rapidxml::xml_node<>* root = doc.allocate_node(rapidxml::node_element, "library");
        root->append_attribute(doc.allocate_attribute("version", "1.0"));
        doc.append_node(root);
        ofstream file_stored(getXmlPath().c_str());
        file_stored << doc;
        file_stored.close();
    } else {
        //read from xml doc
        rapidxml::xml_document<> doc;
        ifstream file(getXmlPath().c_str());
        stringstream buffer;
        buffer << file.rdbuf();
        string content(buffer.str());
        doc.parse<0>(&content[0]);
        rapidxml::xml_node<>* root = doc.first_node("library");
        rapidxml::xml_node<>* artist_node = root->first_node("artist");
        for(; artist_node; artist_node = artist_node->next_sibling()){
            rapidxml::xml_node<>* album_node = artist_node->first_node("album");
            for(; album_node; album_node = album_node->next_sibling()){
                rapidxml::xml_node<>* song_node = album_node->first_node("song");
                stringstream ss;
                for(; song_node; song_node = song_node->next_sibling()){
                    QTableWidgetItem *title_item = new QTableWidgetItem(QString(song_node->first_attribute("title")->value()));
                    QTableWidgetItem *time_item = new QTableWidgetItem(QString(song_node->first_attribute("time")->value()));
                    QTableWidgetItem *artist_item = new QTableWidgetItem(QString(artist_node->first_attribute("id")->value()));
                    QTableWidgetItem *album_item = new QTableWidgetItem(QString(album_node->first_attribute("id")->value()));
                    QTableWidgetItem *location_item = new QTableWidgetItem(QString(song_node->first_attribute("location")->value()));
                    QTableWidgetItem *track_item = new QTableWidgetItem();
                    ss.str("");
                    ss << song_node->first_attribute("track")->value();
                    string track_string = ss.str();
                    int j=0;
                    if(strlen(track_string.c_str())!=0){
                        j+=ss.str().at(strlen(track_string.c_str())-1);
                        if(strlen(track_string.c_str())>1){
                            j+=(track_string.at(strlen(track_string.c_str())-2))*10;
                            if(strlen(track_string.c_str())>2){
                                j+=(track_string.at(strlen(track_string.c_str())-2))*100;
                            }
                        }
                        track_item->setData(Qt::DisplayRole, j);
                    } else {
                        track_item->setText(QString(""));
                    }
                    title_item->setFlags(title_item->flags() ^ Qt::ItemIsEditable);
                    time_item->setFlags(time_item->flags() ^ Qt::ItemIsEditable);
                    artist_item->setFlags(artist_item->flags() ^ Qt::ItemIsEditable);
                    album_item->setFlags(album_item->flags() ^ Qt::ItemIsEditable);
                    track_item->setFlags(track_item->flags() ^ Qt::ItemIsEditable);
                    location_item->setFlags(location_item->flags() ^ Qt::ItemIsEditable);
                    int currentRow = rowCount();
                    insertRow(currentRow);
                    QTableWidgetItem *empty = new QTableWidgetItem;
                    setItem(currentRow, 0, empty);
                    if(nowPlayingContext == currentContext){
                        string this_source = location_item->text().toStdString();
                        if(!strcasecmp(this_source.c_str(),nowPlayingSource.toStdString().c_str())){
                            QTableWidgetItem *nowPlaying = new QTableWidgetItem;
                            nowPlaying->setIcon(QIcon(QPixmap(":playback/icons/speaker_icon.png").scaled(17,17,Qt::KeepAspectRatio)));
                            setItem(currentRow, 0, nowPlaying);
                        }
                    }
                    setItem(currentRow, 1, title_item);
                    setItem(currentRow, 2, time_item);
                    setItem(currentRow, 3, artist_item);
                    setItem(currentRow, 4, album_item);
                    setItem(currentRow, 5, track_item);
                    setItem(currentRow, 6, location_item);
                    item(currentRow,2)->setTextAlignment(Qt::AlignRight|Qt::AlignVCenter);
                    item(currentRow,5)->setTextAlignment(Qt::AlignRight|Qt::AlignVCenter);
                    verticalHeader()->resizeSection(currentRow, 24);
                }
            }
        }
        sortItems(5);
        sortItems(4);
        sortItems(3);
        doc.clear();
    }
}

void MainTable::filter(QString query){
    setRowCount(0);
    if(strncmp(query.toStdString().c_str(),"",sizeof(query.toStdString().c_str()))){
        if(!fileExists(getXmlPath())){
            rapidxml::xml_document<> doc;
            rapidxml::xml_node<>* decl = doc.allocate_node(rapidxml::node_declaration);
            decl->append_attribute(doc.allocate_attribute("version", "1.0"));
            decl->append_attribute(doc.allocate_attribute("encoding", "utf-8"));
            doc.append_node(decl);
            rapidxml::xml_node<>* root = doc.allocate_node(rapidxml::node_element, "library");
            root->append_attribute(doc.allocate_attribute("version", "1.0"));
            doc.append_node(root);
            ofstream file_stored(getXmlPath().c_str());
            file_stored << doc;
            file_stored.close();
        } else {
            rapidxml::xml_document<> doc;
            ifstream file(getXmlPath().c_str());
            stringstream buffer;
            buffer << file.rdbuf();
            string content(buffer.str());
            doc.parse<0>(&content[0]);
            rapidxml::xml_node<>* root = doc.first_node("library");
            rapidxml::xml_node<>* artist_node = root->first_node("artist");
            for(; artist_node; artist_node = artist_node->next_sibling()){
                rapidxml::xml_node<>* album_node = artist_node->first_node("album");
                for(; album_node; album_node = album_node->next_sibling()){
                    rapidxml::xml_node<>* song_node = album_node->first_node("song");
                    stringstream ss;
                    for(; song_node; song_node = song_node->next_sibling()){
                        string song_title = song_node->first_attribute("title")->value();
                        string song_artist = artist_node->first_attribute("id")->value();
                        string song_album = album_node->first_attribute("id")->value();
                        if(stringContains(song_title,query.toStdString()) || stringContains(song_artist,query.toStdString()) || stringContains(song_album,query.toStdString())){
                            QTableWidgetItem *title_item = new QTableWidgetItem(QString(song_node->first_attribute("title")->value()));
                            QTableWidgetItem *time_item = new QTableWidgetItem(QString(song_node->first_attribute("time")->value()));
                            QTableWidgetItem *artist_item = new QTableWidgetItem(QString(artist_node->first_attribute("id")->value()));
                            QTableWidgetItem *album_item = new QTableWidgetItem(QString(album_node->first_attribute("id")->value()));
                            QTableWidgetItem *location_item = new QTableWidgetItem(QString(song_node->first_attribute("location")->value()));
                            QTableWidgetItem *track_item = new QTableWidgetItem();
                            ss.str("");
                            ss << song_node->first_attribute("track")->value();
                            string track_string = ss.str();
                            int j=0;
                            if(strlen(track_string.c_str())!=0){
                                j+=ss.str().at(strlen(track_string.c_str())-1);
                                if(strlen(track_string.c_str())>1){
                                    j+=(track_string.at(strlen(track_string.c_str())-2))*10;
                                    if(strlen(track_string.c_str())>2){
                                        j+=(track_string.at(strlen(track_string.c_str())-2))*100;
                                    }
                                }
                                track_item->setData(Qt::DisplayRole, j);
                            } else {
                                track_item->setText(QString(""));
                            }
                            title_item->setFlags(title_item->flags() ^ Qt::ItemIsEditable);
                            time_item->setFlags(time_item->flags() ^ Qt::ItemIsEditable);
                            artist_item->setFlags(artist_item->flags() ^ Qt::ItemIsEditable);
                            album_item->setFlags(album_item->flags() ^ Qt::ItemIsEditable);
                            track_item->setFlags(track_item->flags() ^ Qt::ItemIsEditable);
                            location_item->setFlags(location_item->flags() ^ Qt::ItemIsEditable);
                            int currentRow = rowCount();
                            insertRow(currentRow);
                            if(nowPlayingContext == currentContext){
                                string this_source = location_item->text().toStdString();
                                if(!strcasecmp(this_source.c_str(),nowPlayingSource.toStdString().c_str())){
                                    QTableWidgetItem *nowPlaying = new QTableWidgetItem;
                                    nowPlaying->setIcon(QIcon(QPixmap(":playback/icons/speaker_icon.png").scaled(17,17,Qt::KeepAspectRatio)));
                                    setItem(currentRow, 0, nowPlaying);
                                }
                            }
                            setItem(currentRow, 1, title_item);
                            setItem(currentRow, 2, time_item);
                            setItem(currentRow, 3, artist_item);
                            setItem(currentRow, 4, album_item);
                            setItem(currentRow, 5, track_item);
                            setItem(currentRow, 6, location_item);
                            item(currentRow,2)->setTextAlignment(Qt::AlignRight|Qt::AlignVCenter);
                            item(currentRow,5)->setTextAlignment(Qt::AlignRight|Qt::AlignVCenter);
                            verticalHeader()->resizeSection(currentRow, 24);
                        }
                    }
                }
            }
            sortItems(5);
            sortItems(4);
            sortItems(3);
            doc.clear();
        }
    } else {
        populate();
    }
}

void MainTable::filterByArtist(QString artist_){
    string artist = artist_.toStdString();
    setRowCount(0);
    if(strncmp(artist.c_str(),"",sizeof(artist.c_str()))){
        if(!fileExists(getXmlPath())){
            rapidxml::xml_document<> doc;
            rapidxml::xml_node<>* decl = doc.allocate_node(rapidxml::node_declaration);
            decl->append_attribute(doc.allocate_attribute("version", "1.0"));
            decl->append_attribute(doc.allocate_attribute("encoding", "utf-8"));
            doc.append_node(decl);
            rapidxml::xml_node<>* root = doc.allocate_node(rapidxml::node_element, "library");
            root->append_attribute(doc.allocate_attribute("version", "1.0"));
            doc.append_node(root);
            ofstream file_stored(getXmlPath().c_str());
            file_stored << doc;
            file_stored.close();
        } else {
            rapidxml::xml_document<> doc;
            ifstream file(getXmlPath().c_str());
            stringstream buffer;
            buffer << file.rdbuf();
            string content(buffer.str());
            doc.parse<0>(&content[0]);
            rapidxml::xml_node<>* root = doc.first_node("library");
            rapidxml::xml_node<>* artist_node = root->first_node("artist");
            for(; artist_node; artist_node = artist_node->next_sibling()){
                string current_artist = artist_node->first_attribute("id")->value();
                if(!strcasecmp(current_artist.c_str(), artist.c_str())){
                    rapidxml::xml_node<>* album_node = artist_node->first_node("album");
                    for(; album_node; album_node = album_node->next_sibling()){
                        rapidxml::xml_node<>* song_node = album_node->first_node("song");
                        stringstream ss;
                        for(; song_node; song_node = song_node->next_sibling()){
                            QTableWidgetItem *title_item = new QTableWidgetItem(QString(song_node->first_attribute("title")->value()));
                            QTableWidgetItem *time_item = new QTableWidgetItem(QString(song_node->first_attribute("time")->value()));
                            QTableWidgetItem *artist_item = new QTableWidgetItem(QString(artist_node->first_attribute("id")->value()));
                            QTableWidgetItem *album_item = new QTableWidgetItem(QString(album_node->first_attribute("id")->value()));
                            QTableWidgetItem *location_item = new QTableWidgetItem(QString(song_node->first_attribute("location")->value()));
                            QTableWidgetItem *track_item = new QTableWidgetItem();
                            ss.str("");
                            ss << song_node->first_attribute("track")->value();
                            string track_string = ss.str();
                            int j=0;
                            if(strlen(track_string.c_str())!=0){
                                j+=ss.str().at(strlen(track_string.c_str())-1);
                                if(strlen(track_string.c_str())>1){
                                    j+=(track_string.at(strlen(track_string.c_str())-2))*10;
                                    if(strlen(track_string.c_str())>2){
                                        j+=(track_string.at(strlen(track_string.c_str())-2))*100;
                                    }
                                }
                                track_item->setData(Qt::DisplayRole, j);
                            } else {
                                track_item->setText(QString(""));
                            }
                            title_item->setFlags(title_item->flags() ^ Qt::ItemIsEditable);
                            time_item->setFlags(time_item->flags() ^ Qt::ItemIsEditable);
                            artist_item->setFlags(artist_item->flags() ^ Qt::ItemIsEditable);
                            album_item->setFlags(album_item->flags() ^ Qt::ItemIsEditable);
                            track_item->setFlags(track_item->flags() ^ Qt::ItemIsEditable);
                            location_item->setFlags(location_item->flags() ^ Qt::ItemIsEditable);
                            int currentRow = rowCount();
                            insertRow(currentRow);
                            QTableWidgetItem *empty = new QTableWidgetItem;
                            setItem(currentRow, 0, empty);
                            if(nowPlayingContext == currentContext){
                                string this_source = location_item->text().toStdString();
                                if(!strcasecmp(this_source.c_str(),nowPlayingSource.toStdString().c_str())){
                                    QTableWidgetItem *nowPlaying = new QTableWidgetItem;
                                    nowPlaying->setIcon(QIcon(QPixmap(":playback/icons/speaker_icon.png").scaled(17,17,Qt::KeepAspectRatio)));
                                    setItem(currentRow, 0, nowPlaying);
                                }
                            }
                            setItem(currentRow, 1, title_item);
                            setItem(currentRow, 2, time_item);
                            setItem(currentRow, 3, artist_item);
                            setItem(currentRow, 4, album_item);
                            setItem(currentRow, 5, track_item);
                            setItem(currentRow, 6, location_item);
                            item(currentRow,2)->setTextAlignment(Qt::AlignRight|Qt::AlignVCenter);
                            item(currentRow,5)->setTextAlignment(Qt::AlignRight|Qt::AlignVCenter);
                            verticalHeader()->resizeSection(currentRow, 24);
                        }
                    }
                }
            }
            sortItems(5);
            sortItems(4);
            sortItems(3);
            doc.clear();
        }
    }
}

void MainTable::setNowPlayingIcon(string newSource){
    clearNowPlayingIcon();
    int count = rowCount();
    for(int i=0; i<count; i++){
        string this_source = item(i,6)->text().toStdString();
        if(!strcasecmp(newSource.c_str(),this_source.c_str())){
            QTableWidgetItem *nowPlaying = new QTableWidgetItem;
            nowPlaying->setIcon(QIcon(QPixmap(":playback/icons/speaker_icon.png").scaled(17,17,Qt::KeepAspectRatio)));
            setItem(i, 0, nowPlaying);
            if(i%2==0){
                item(i,0)->setBackgroundColor(QColor(76,88,102));
            } else {
                item(i,0)->setBackgroundColor(QColor(102,113,127));
            }
            break;
        }
    }
}

void MainTable::clearNowPlayingIcon(){
    int count = rowCount();
    for(int i=0; i<count; i++){
        QTableWidgetItem *empty = new QTableWidgetItem;
        setItem(i,0,empty);
        if(i%2==0){
            item(i,0)->setBackgroundColor(QColor(76,88,102));
        } else {
            item(i,0)->setBackgroundColor(QColor(102,113,127));
        }
    }
}
