#ifndef TITLEBAR_H
#define TITLEBAR_H

#include <QtGui>
#include <QWidget>

class TitleBar : public QWidget{
    Q_OBJECT
public:
    TitleBar(QWidget *parent){
        //set some properties for this widget
        setAutoFillBackground(true);
        setFixedHeight(23);
        setStyleSheet("QWidget{"
                      "border-width:0px;"
                      "background-color:qlineargradient(spread:pad, x1:0.516778, y1:0.08, x2:0.518, y2:0.983,"
                      "stop:0.241379 rgba(94, 94, 94, 255), stop:0.551724 rgba(66, 66, 66, 255));"
                      "spacing:0px;"
                      "margins:0px;"
                      "}");
        setContentsMargins(0,0,0,0);

        //setup top label
        topLabel = new QLabel;
        topLabel->setText(QString("Audyssey"));
        topLabel->setAlignment(Qt::AlignHCenter);
        topLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        topLabel->setStyleSheet("QLabel{"
                             "padding:3px;"
                             "font-size:14;"
                             "font-weight:800;"
                             "color:rgba(220, 220, 220);"
                             "}");
        QFont newFont("Avenir", 14);
        newFont.setWeight(800);
        topLabel->setFont(newFont);

        //now window buttons
        maximize = new QPushButton(this);
        minimize = new QPushButton(this);
        close = new QPushButton(this);

        closeIcon = style()->standardPixmap(QStyle::SP_TitleBarCloseButton);
        close->setIcon(closeIcon);
        close->setStyleSheet("QPushButton{"
                             "background-color:transparent;"
                             "}");
        maxIcon = style()->standardPixmap(QStyle::SP_TitleBarMaxButton);
        maximize->setIcon(maxIcon);
        maximize->setStyleSheet("QPushButton{"
                                "background-color:transparent;"
                                "}");
        minIcon = style()->standardPixmap(QStyle::SP_TitleBarMinButton);
        minimize->setIcon(minIcon);
        minimize->setStyleSheet("QPushButton{"
                                "background-color:transparent;"
                                "}");
        restoreIcon = style()->standardPixmap(QStyle::SP_TitleBarNormalButton);

        minimize->setFixedSize(20,20);
        close->setFixedSize(20,20);
        maximize->setFixedSize(20,20);

        connect(close, SIGNAL( clicked() ), parent, SLOT(close()));
        connect(minimize, SIGNAL( clicked() ), this, SLOT(showSmall() ) );
        connect(maximize, SIGNAL( clicked() ), this, SLOT(showMaxRestore() ) );

        QLabel *spacer = new QLabel(this);
        spacer->setFixedWidth(60);
        //put together toolbar
        QToolBar *topBar = new QToolBar(this);
        topBar->addWidget(close);
        topBar->addWidget(minimize);
        topBar->addWidget(maximize);
        topBar->addWidget(topLabel);
        topBar->addWidget(spacer);
        topBar->setStyleSheet("QToolBar{"
                           "border:0px;"
                           "spacing:0px;"
                           "}");
        topBar->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        QHBoxLayout *top = new QHBoxLayout(this);
        top->addWidget(topBar);
        top->setContentsMargins(0,0,0,0);
        top->setSpacing(0);
        maxNormal = false;
    }
    QPixmap maxIcon, restoreIcon, closeIcon, minIcon;
    QLabel *topLabel;
    QLineEdit *searchBar;
    QPushButton *minimize, *maximize, *close;
    bool maxNormal;

public slots:
    void showSmall(){
        if(maxNormal){
            parentWidget()->showNormal();
            maxNormal = !maxNormal;
            maximize->setIcon(maxIcon);
        }
        parentWidget()->showMinimized();
    }
    void showMaxRestore(){
        if (maxNormal) {
            parentWidget()->showNormal();
            maxNormal = !maxNormal;
            maximize->setIcon(maxIcon);
        } else {
            parentWidget()->showFullScreen();
            maxNormal = !maxNormal;
            maximize->setIcon(restoreIcon);
        }
    }

protected:
    void mousePressEvent(QMouseEvent *me){
        startPos = me->globalPos();
        clickPos = mapToParent(me->pos());
    }
    void mouseMoveEvent(QMouseEvent *me){
        if(maxNormal){
            return;
        } else {
            parentWidget()->move(me->globalPos() - clickPos);
        }
    }

private:
    QPixmap restorePix, maxPix;
    QPoint startPos;
    QPoint clickPos;
};

#endif // TITLEBAR_H
